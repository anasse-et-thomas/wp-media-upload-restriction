<?php

/**
 * Max width of an image in pixels.
 * Is used to scale down images that are too big when uploaded. The original big image will persist in the file system but will not be used by WordPress.
 */
const IMAGE_MAX_WIDTH = 1500;

/**
 * Max sizes of media files in bytes.
 * Is used to deny the upload of a file if too big.
 * Keys are the first part of mimetypes.
 * A missing key is treated as an absence of restriction.
 */
const MEDIA_MAX_SIZE_BY_TYPE = [
	'image' => 500 * 1024, // 500 kio
	'video' => 40 * 1024 * 1024, // 40 Mio
];