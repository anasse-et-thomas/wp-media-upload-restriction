# WP Media Upload Restriction

Wordpress plugin which applies different restrictions to uploading files to the media library

## To-do list

* French translation
* Plugin singleton + namespace
* Media name filtering
* Display SI units if writer and binary units if admin
* Live configuration of the plugin