<?php

/**
 * Plugin Name: WP Media Upload Restriction
 * Description: Applies different restrictions to uploading files to the media library
 * Version: 0.1
 * Author: Incite Communication
 * Author URI: https://incite-communication.fr/
 * Licence: GPLv3
 * Licence URI: https://www.gnu.org/licenses/gpl-3.0.en.html
 */

include_once plugin_dir_path( __FILE__ ) . 'config.php';

/**
 * Scale down an uploaded image if its too wide.
 * 
 * The original big image will persist in the file system but will not be used by WordPress. The one used by WordPress will be suffixed by "-scaled".
 * Thumbnails are generated from the scaled image.
 * Use IMAGE_MAX_WIDTH from config.php.
 * 
 * Is hooked to the WordPress filter "big_image_size_threshold" which defines the parameters and return.
 * 
 * @param  int       $_         The threshold value in pixels. (Not used here.)
 * @param  array     $imagesize Indexed array of the image width and height in pixels. (0 is the image width, 1 is the image height.)
 * @return int|false            The max size threshold in pixels, false if disabled.
 */
function resmed_on_upload_resize_image($_, $imagesize) {
	$width = $imagesize[0];
	$height = $imagesize[1];

	if ($width == 0 || $height == 0) return 0;
	
	if ($width >= $height) {
		// The image is in landscape or square format
		return IMAGE_MAX_WIDTH;
	} else {
		// The image is in portrait format
		// Cross product to determine the maximum height that corresponds to the desired width
		return $height / $width * IMAGE_MAX_WIDTH;
	}
}
add_filter('big_image_size_threshold', 'resmed_on_upload_resize_image', 999, 2);

/**
 * Deny media upload if its size is too big.
 * Each type (first part of mimetype i.e. image, video...) can have a different value.
 * Use MEDIA_MAX_SIZE_BY_TYPE from config.php.
 * 
 * Is hooked to the WordPress filter "wp_handle_upload_prefilter" which defines the parameters and return.
 * 
 * @param  array $file An array of data for a single file. Contains the following values:
 *                     string 'type'  Mimetype of the file.
 *                     int    'size'  Size of the file in bytes.
 * @return array       The array of filtered data for $file. Contains the following values:
 *                     mixed  'error' An error code (int), an error description (string) or no error (not set/falsy). 
 */
function resmed_on_upload_filter_media($file) {
	$type = explode('/', $file['type'])[0];
	
	// If the max size is defined for the type AND if the file size is bigger than this max size
	if (isset(MEDIA_MAX_SIZE_BY_TYPE[$type]) && $file['size'] > MEDIA_MAX_SIZE_BY_TYPE[$type]) {
		$file['error'] = sprintf(
			'Le fichier "%s" est trop lourd (%s). Les fichiers de type %s ne doivent pas dépasser %s. Veuillez suivre les consignes du tableau de bord.',
			$file['name'],
			resmed_human_readable_file_size($file['size']),
			$type,
			resmed_human_readable_file_size(MEDIA_MAX_SIZE_BY_TYPE[$type], 0)
		);
	}
	
	return $file;
}
add_filter('wp_handle_upload_prefilter', 'resmed_on_upload_filter_media');

/**
 * Convert a number of bytes into a human-readable string.
 * The conversion uses the binary units system (KiB, MiB...) but display the numbers as the SI system (kB, MB...). This is an intended error that mimics the behavior of Windows Explorer to simplify the comprehension of uninformed users.
 * 
 * @param  int    $bytes    Number of bytes to convert.
 * @param  int    $decimals Number of decimals to show.
 * @return string           Number of bytes in a human-readable format.
 */
function resmed_human_readable_file_size($bytes, $decimals = 2){
	$prefixes = ['', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'];

	$factor = floor(log($bytes, 1024));
	if ($factor >= count($prefixes)) $factor = count($prefixes) - 1;

	return sprintf(
		"%.{$decimals}f %sB",
		$bytes / pow(1024, $factor),
		$prefixes[$factor]
	);
}